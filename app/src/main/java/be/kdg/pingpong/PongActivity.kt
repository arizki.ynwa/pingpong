package be.kdg.pingpong

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.widget.ImageView

class PongActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pong)

        addEventHandlers()
    }

    private fun addEventHandlers() {
        val ivPong = findViewById<ImageView>(R.id.pongImageView)
        ivPong.setOnClickListener {
            val intent = Intent(applicationContext, PingActivity::class.java)
            startActivity(intent)
        }
    }
}
