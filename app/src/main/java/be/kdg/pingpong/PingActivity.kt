package be.kdg.pingpong

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.widget.ImageView

class PingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ping)
        addEventHandlers()
    }

    private fun addEventHandlers() {
        val ivPong = findViewById<ImageView>(R.id.pingImageView)
        ivPong.setOnClickListener {
            val intent = Intent(applicationContext, PongActivity::class.java)
            startActivity(intent)
        }
    }
}
